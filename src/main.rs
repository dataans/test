#[actix_web::main]
async fn main() -> std::io::Result<()> {
    test_web_service::app::start_app().await
}
