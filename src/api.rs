use actix_web::get;
use actix_web::http::StatusCode;
use actix_web::{HttpResponse, Responder};
use redis::{AsyncCommands, Client};

#[get("/")]
pub async fn root_health() -> impl Responder {
    HttpResponse::Ok().body("root: ok.")
}

#[get("/api/v1/test/health")]
pub async fn health() -> impl Responder {
    HttpResponse::Ok().body("test ok.")
}

#[get("/create_test_file")]
pub async fn create_file() -> impl Responder {
    use std::io::prelude::*;

    println!("start");
    match std::fs::File::create("/data/test/foo.txt") {
        Ok(mut file) => match file.write_all(b"test file creation") {
            Ok(_) => {
                println!("data written");
                HttpResponse::Ok().body("file created")
            }
            Err(err) => HttpResponse::Ok().body(format!("{:?}", err)),
        },
        Err(err) => HttpResponse::Ok().body(format!("{:?}", err)),
    }
}

#[get("/api/v1/redis")]
pub async fn get_redis_value() -> impl Responder {
    match Client::open("redis://redis") {
        Ok(redis) => match redis.get_async_connection().await {
            Ok(mut connection) => {
                let value: Result<String, _> = connection.get("pasha".to_owned()).await;
                println!("value: {:?}", value);
                HttpResponse::Ok().body(format!("{:?}", value))
            }
            Err(error) => {
                println!("{:?}", error);
                HttpResponse::Ok()
                    .status(StatusCode::INTERNAL_SERVER_ERROR)
                    .body("sad:(")
            }
        },
        Err(error) => {
            println!("{:?}", error);
            HttpResponse::Ok()
                .status(StatusCode::INTERNAL_SERVER_ERROR)
                .body("sad:(")
        }
    }
}
