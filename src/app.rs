use actix_cors::Cors;
use actix_web::{App, HttpServer};

use crate::api::{create_file, get_redis_value, health, root_health};

pub async fn start_app() -> std::io::Result<()> {
    HttpServer::new(move || {
        App::new()
            .wrap(actix_web::middleware::Logger::default())
            .wrap(
                Cors::default()
                    .expose_any_header()
                    .supports_credentials()
                    .allow_any_header()
                    .allow_any_origin()
                    .allow_any_method(),
            )
            .service(root_health)
            .service(health)
            .service(get_redis_value)
            .service(create_file)
    })
    .bind("0.0.0.0:8081")?
    .run()
    .await
}
